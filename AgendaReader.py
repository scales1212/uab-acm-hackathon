
import PyPDF2
import requests
from pathlib import Path
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


def main():
    agenda_src = "https://www.birminghamalcitycouncil.org/wp-content/uploads/2019/03/BIRMINGHAM-CITY-COUNCIL-MEETING-AGENDA-April-2-2019.pdf"
    download_pdf(agenda_src)


def download_pdf(url):
    filename = Path('currentAgenda.pdf')
    response = requests.get(url)
    filename.write_bytes(response.content)


# Reads and tokenizes a PDF into a list of keywords. Keyword list is returned.
def agenda_read(filename):
    pdfFileObj = open(filename, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

    numPages = pdfReader.numPages
    count = 0
    text = ""

    while count < numPages:
        pageObj = pdfReader.getPage(count)
        count += 1
        text += pageObj.extractText()

    tokens = word_tokenize(text)
    punctuations = ['(', ')', ';', ':', '[', ']', ',']

    stop_words = stopwords.words('english')
    keywords = [word for word in tokens if not word in stop_words and not word in punctuations]

    return keywords
