from collections import Counter


class Keyword:

    frequencies = {}
    threshold = 1

    def __init__(self, values):
        
        self.values = values
    """
    Method builds a dictionary from the text with keys as words and their 
    frequency as the value
    """
    def buildFrequencies(self):
        words = []
        for word in self.values:
            word = word.lower()
            words.append(word)
        counts = Counter(words)
        self.frequencies = counts
    """
    Method counts number of times each user's keyword is mentioned in text then
    returns the number one most mentioned keyword, if the total count passes a 
    pre-set threshold
    input: userKeys (list)
    output: topKW (str)
    """
    def countForUser(self, userKeys):
        count = 0
        topKW = userKeys[0]

        for key in self.frequencies:
            for word in userKeys:
                if word == key:
                    count += self.frequencies[key]

                    if self.frequencies[key] > self.frequencies[topKW]:
                        topKW = key

        if count > self.threshold:
            return topKW
        else:
            return "Not enough"
