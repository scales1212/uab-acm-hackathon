
class User:
    
    def __init__(self,firstName, lastName, email, Keywords):
        
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.Keywords = Keywords
    
    def setKeywords(self, keys):
        
        for i in range(len(keys)):
            if keys[i] not in self.Keywords:
                self.Keywords.append(keys[i])
    
    def setEmail(self, newEmail):
    
        self.email = newEmail
    """
    Method writes any new users data to the txt file
    """
    def writeNewUser(self):

        currentUsers = open("UserData.txt", "a")
        currentUsers.write(self.firstName + ',' + self.lastName + ',' + self.email + ',' + self.Keywords)

        currentUsers.close()

