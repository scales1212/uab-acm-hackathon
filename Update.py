from Keyword import *
from User import *
from AgendaReader import *
from AgendaMailer import *
import re


def Update():
    URL =====
    
    
    filename = 'agenda1.pdf'
    
    # sets up the collection frequency dictionary after scrapping text data
    values = agenda_read(filename)
    Key = Keyword(values)
    Key.buildFrequencies()
    # freq = Key.frequencies
    
    # reads in current users
    userList = []
    currentUsers = open(r"UserData.txt", "r")
    
    for line in currentUsers:
        line = line.rstrip('\n')
        linestr = line.split(',')
        temp = User(linestr[0], linestr[1], linestr[2], linestr[3:])
        userList.append(temp)
    currentUsers.close()

    # iterates over users and, if they pass the threshold and should recieve an 
    # email, first checks with re to see if email is valid then sends email using
    # AgendaMailer()
    for i in userList:
        
        Keys = i.Keywords
        result = Key.countForUser(Keys)

        if result != "Not enough":
            pattern = re.compile("^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$")
            emailIsValid = pattern.match(i.email)
            if emailIsValid:
                print(i.email, " ", result)
                am = AgendaMailer()
                am.send_council_notice(i, result.capitalize(), '4-2-19')
            else:
                print("Invalid email: skipping user " + i.firstName + " " + i.lastName)

