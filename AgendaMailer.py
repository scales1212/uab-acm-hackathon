import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

'''
This class establishes a connection with a dedicated email account over encrypted SMTP, and is responsible for sending
notification emails to registered users.
'''
class AgendaMailer():

    """
    Initializes the AgendaMailer object with server and port information for a dedicated GMail account.
    """
    def __init__(self):
        self.port = 465
        self.password = 'bhammailer1'

        self.context = ssl.create_default_context()
        self.smtp_server = "smtp.gmail.com"
        self.sender_email = "bhamcouncilmailer@gmail.com"

        self.subject = "Birmingham city council notification"

    """
    Sends a notification to a registered user regarding council activity related to user interest.
    """
    def send_council_notice(self, user, result, date):

        body = "Birmingham city council will be discussing issues which you have expressed interest in on " + date + "."
        body2 = "\nIdentified interest which will be discussed: " + result
        body3 = "\n\nThe Birmingham City Council meets every Tuesday at 9:30 a.m. (excluding holidays). "
        body4 = "\nAddress: 710 North 20th Street, Birmingham, Alabama"
        body5 = "\nLocated in Birmingham city hall, 3rd floor."

        msg = MIMEMultipart()
        msg['From'] = self.sender_email
        msg['To'] = user.email
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = self.subject
        body = MIMEText(body + body2 + body3 + body4 + body5, 'plain')
        msg.attach(body)

        receiver_email = user.email
        with smtplib.SMTP(self.smtp_server) as server:
            server.starttls(context=self.context)
            server.ehlo()
            server.login(self.sender_email, self.password)
            server.sendmail(self.sender_email, receiver_email, msg.as_string())
            server.close()


